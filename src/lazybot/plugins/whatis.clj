(ns lazybot.plugins.whatis
  (:use [lazybot registry utilities]
        [somnium.congomongo :only [fetch fetch-one insert! destroy!]])
  (:require [clojure.string :as string]))

(comment
  (fetch :whatis)
  )
(defn tell-about [what com-m]
  (send-message com-m
                (str what
                     (if-let [result (fetch-one :whatis :where {:subject what})]
                       (str " is " (:is result))
                       " does not exist in my database."))))

(def learn-responses
  ["My memory is more powerful than M-x butterfly. I won't forget it."
   "Got it, boss."
   "Sure, let me just put that in my MongoDB for you."
   "If you insist."
   "Okay, learned."
   "Learned. Someone is regularly backing up my DB, right?"
   "Got it. But you realize probably nobody will look that up unless they're doing @randomfact out of boredom."
   "You don't say?"
   "For real?  Okay."
   "I'll never forget that. (Unless you @forget it.)"
   "The Moving Finger writes; and, having writ, Moves on: nor all thy Piety nor Wit Shall lure it back to cancel half a Line, Nor all thy Tears wash out a Word of it.  But @forget works."
   ])

(defn prepare-regex [re-str]
  (re-pattern (str "(?i).*" re-str ".*")))

(defn regex-search [query]
  ; would be nice if we could do the regexing on the mongo server instead of fetching it all
  (filter (fn [ent] (and (:subject ent)
                         (re-matches query (:subject ent))))
          (fetch :whatis)))

(defn regex-search-ui [args]
  (if-let [query (first args)]
    (let [results (regex-search (prepare-regex query)) ]
      (case (count results)
        0 "No results found."
        1 (let [{:keys [subject is]} (first results)] (str subject " is " is))
        (str "Found the following keys: " (string/join ", " (map :subject results)))))
    "Usage: @whatis~ <regex>"))

(defplugin
  (:cmd
   "Teaches the bot a new thing. It takes a name and whatever you want to assign the name
   to. For example: $learn me a human being."
   #{"learn"}
   (fn [{:keys [args nick] :as com-m}]
     (let [[subject & is] args
           is-s (apply str (interpose " " is))]
       (do
         ; look up existing fact?
         ; if we allow multiple defs for a fact, @whatis will have to account for that
         ;; (destroy! :whatis {:subject subject})
         (insert! :whatis {:subject subject
                           :is is-s
                           :nick nick
                           :timestamp (java.util.Date.)
                           })
         (send-message com-m (rand-nth learn-responses))))))


  #_(:cmd
    "Teaches the bot a new fact. Facts must include the word 'is' or 'are'.
    Example: `@learn my IRC bot is being a dick` would use 'my IRC bot' as a key and would store the phrase 'is being a dick'
    for later lookup, use `@whatis my IRC bot`."
    #{"learn2"}
    (fun [{:keys [args] :as com-m}]
        (let [input        (apply str (interpose " " is))
              [key phrase] (map string/trim
                                (string/split #"\b(is|are)\b" 2))]
          (do
            (if (and key phrase)
              (do
                ;; TODO: show them the old fact if this is a replace
                ;; possibly keep storing the old fact
                ;; (destroy! :whatis {:subject subject})
                (insert! :whatis {:subject key, :is phrase})
                (send-message com-m (rand-nth learn-responses)))
              (do
                (send-message com-m "I can only learn phrases including 'is' of the form: @learn my example is pretty simple")
                ))
            ))))

   (:cmd
    "Pass it a key, and it will tell you what is at the key in the database."
    #{"whatis" "what-is"}
    (fn [{[what] :args :as com-m}]
      (tell-about what com-m)))

   (:cmd
    "Pass it a key, and it will tell the recipient what is at the key in the database via PM
Example - $tell G0SUB about clojure"
    #{"tell"}
    (fn [{[who _ what] :args :as com-m}]
      (when what
        (tell-about what (assoc com-m :channel who)))))

   (:cmd
    "Forgets the value of a key."
    #{"forget"}
    (fn [{[what] :args :as com-m}]
      ; this would also need updating to deal with plural facts
      (do (destroy! :whatis {:subject what})
          (send-message com-m (str "If " what " was there before, it isn't anymore. R.I.P.")))))

   (:cmd
    "Gets a random value from the database."
    #{"rwhatis" "randomfact" "random-fact"}
    (fn [com-m]
      (let [what (-> :whatis fetch rand-nth :subject)]
        (tell-about what com-m))))

   (:cmd
     "Search for a whatis entry via regex"
     #{"whatis~" "what-is~" "what-is-regex"}
     (fn [{:keys [nick args] :as com-m}]
       (send-message com-m (prefix nick (regex-search-ui args)))))

  (comment
    ; working on a command to show metadata about a fact
    ; such as author and timestamp
    (:cmd
    "get info about a fact"
    #{"factinfo" "fact-info"}
    (fn [{[what] :args :as com-m}]
      ()
      )
    ))

   (:indexes [[:subject]]))

(comment
  (def alles (fetch :whatis))
  (count alles)
  (first alles)
  (filter (fn [ent] (and (:subject ent)
                         (.contains (.toLowerCase (:subject ent)) "audit")))
          alles)
  (.contains "foobar" "oba")
  (.toLowerCase "yUp")
  (regex-search (prepare-regex "cam"))
  (regex-search-ui ["cam"])
  (regex-search-ui ["pscam"])
  (regex-search-ui [])
  )
