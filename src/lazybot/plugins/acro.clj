(ns lazybot.plugins.acro
  (:require [clojure.string :as string])
  (:use lazybot.registry
        [lazybot.utilities :only [prefix]]))

;; contributed by J. Andrew Thompson 2013-08-22

; last word must be a noun
; any other word can be either type
(def nouns
  ["Abacus" "Agony"
   "Birthlube" "Bacon" "Belly" "Belch" "Butter" "Broker" "BYOBB"
   "Children" "Candy" "Cankle" "Campus" "Conduit"
   "Danish" "Department"
   "Emo" "Elbow-Grease" "Elf" "Enterprise"
   "Fandango" "Festivus" "Functor" "Floppy" "Flagellum"
   "Glassfish" "Gangster" "Gonad"
   "Hair" "Horns" "Haggler"
   "Irksome" "Interweb" "Illinois" "Interception" "Integration"
   "Java" "Jawa" "JVM"
   "K-Mart" "Kwanzaa" "Kevlar" "Knowledge"
   "Liquid" "Lothlorien" "LEAP"
   "Meat" "Manhood" "Monad" "Mangler" "Monkey" "Muffin" "Memory" "McGuffin" "Mastication" "Manager"
   "Neptha" "Ninja" "NSA" "Naughtiness"
   "Oprah" "Opera" "Octuplet" "Oracle"
   "Panzy" "Party" "Polyglot"
   "QWERTY" "Quigley" "Quenya" "Quahog" "Query" "Quinine" "Questionaire"
   "Ragamuffin" "Runt" "Rumination" "Revision"
   "Steak" "Swag" "Spline" "Spleen" "Splash" "Sneeze" "Solution"
   "Turd" "Torpedo" "Tupperware" "Tiddly-Wink" "Tool"
   "Usurper" "Underdunk"
   "Verification" "Validation" "Versimillitude"
   "Wonk" "Wanker" "Wiretap"
   "Xenophobe" "X-ray"
   "Y'all" "Yeller" "Yourself" "Yeomen"
   "Zombie" "Zygote" "Zeppelin"
   ])

(def adjectives
  ["Abismal" "Anti" "Asymmetric" "Antebellum" "Antediluvian" "Alien" "Administrative" "Asynchronous" "Anti-tellharsic" "Apache"
   "Byzantine" "Baffling" "Barge-like" "Belligerant"
   "Crappy" "Cynical" "Conical" "Candied" "Camouflaged" "Compressed"
   "Damnable" "Danish" "Draconian" "Darned"
   "Emo" "Elvish" "Eldritch" "Electrical" "Effective"
   "Freakin'" "Floppy" "Flagrant" "Fortuituous"
   "Great" "Granulated" "Gargantuan" "Gelatinous" "Gzipped"
   "Hella" "Hellacious" "Hoary" "Head-mounted"
   "Irksome" "Impossible" "Improbable" "Implausible" "Inconceivable"
   "Jolly" "Jank" "Jim-dandy"
   "Krunk" "Kwaaazy"
   "Liquid" "Lucky" "Lugubrious" "Loquatious" "Laughable" "Lauded"
   "Meaty" "Mega" "Manly" "Monadic" "Mangled" "My" "Mucho" "Mondo"
   "Neptha" "Ninja" "NSA" "Naughtiness"
   "Ostracised" "One" "Optical" "Organic" "Obsequious" "Ornery"
   "Putrid" "Panzy" "Practical" "Placid"
   "Quiet" "Quiescent" "Quilted" "Quenching" "Quotidian" "Quivering"
   "Random" "Round" "Righteous" "Riticulated"
   "Stupid" "Sardonic" "Salacious" "Spring-loaded"
   "Toasty" "Titillating" "Tellharsic" "Turgid"
   "Ubiquitous" "Unlikely" "Uppity" "Urbane" "Uncompressed"
   "Verily" "Venomous" "Vapid" "Vaporized" "Vacuous"
   "Wanky" "Wonky" "Wonderful" "Whiskey" "Wry"
   "X-hilerating" "Xenomorphed"
   "Young" "Your" "Yucky"
   "Zany" "Zealous" "Zigzagged" "Zippy" "Zirconium"
   ])

(defn make-first-letter-map [words]
  (group-by (comp string/upper-case first) words))

;; (def adj-dict (make-first-letter-map adjectives))
(def noun-dict (make-first-letter-map nouns))
(def all-dict (make-first-letter-map (concat adjectives nouns)))

(defn make-acronym [word]
  (let [prefix (for [c (butlast word)]
                 (rand-nth (get all-dict (string/upper-case c))))]
    (string/join " "
                 (concat prefix [(rand-nth (get noun-dict (string/upper-case (last word))))]))))

(comment
  (make-acronym "yummo")
  )

(defplugin
  (:cmd
    "Get the definition for an acronymn."
    #{"acro" "whatsa"}
    (fn [{:keys [bot nick channel args] :as com-m}]
      (send-message com-m (prefix nick (make-acronym (first args)))))))

