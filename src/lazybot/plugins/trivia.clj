(ns lazybot.plugins.trivia
  (:require [clojure.string :as string]
            [clj-http.client :as client])
  (:use [lazybot registry utilities]
        [somnium.congomongo :only [fetch insert!]]
        [clj-time.core :only [now]])
  )

; trivia questions from http://snapnjacks.com/getq.php

; related links:
; http://snapnjacks.com/qstats.php
; http://www.stealthbot.net/board/index.php?showtopic=12827
; http://snapnjacks.com/SnapNJacksTrivia.txt
; http://snapnjacks.com/SnapNJacksTrivia0.846.txt
; https://github.com/uakfdotb/gcb/blob/master/bin/plugins/gcb/trivia.java

; wish this APi ever worked
; https://www.mashape.com/privnio/trivia

;; (def TRIVIA-URL "http://snapnjacks.com/getq.php")
(def TRIVIA-URL "http://snapnjacks.com/getq.php?ctg=")
;; (def TRIVIA-URL "http://snapnjacks.com/getq.php?ctg=-Blizzard,-DotA")
;; (def TRIVIA-URL "http://snapnjacks.com/getq.php?ctg=LOTR")
(def PROMPTS 3)
(def PROMPT-INTERVAL 10000) ; msec
(def DEFAULT-CATEGORIES "-Blizzard,-Dota")

(def EXCLAMATIONS
  ["DING DING DING, we have a winner!"
   "HOT DIGGITY DANG!"
   "ACH DU LIEBER!"
   "BY ODIN'S BEARD!"
   "GEE WILICKERS!"
   "WELL SLAP ME SILLY AND CALL ME SUSANBOT!"
   ; most responses courtesy Hermes Conrad
   "SWEET GORILLA OF MANILA!"
   "SWEET THREE-TOED SLOTH OF ICE PLANET HOTH!"
   "SWEET YETI OF THE SERENGETI!"
   "SWEET FILE-NOT-FOUND OF PUGET SOUND!"
   "SWEET ORCA OF MAJORCA!"
   "SWEET SHE-CATTLE OF SEATTLE!"
   "SWEET CONINCIDENCE OF PORT-AU-PRINCE!"
   "SWEET LION OF ZION!"
   "SWEET MANATEE OF GALILEE!"
   "SWEET LLAMAS OF THE BAHAMAS!"
   "GREAT COW OF MOSCOW!"
   "SWEET GIANT ANTEATER OF SANTA ANITA!"
   "SWEET SACRED BOA OF WESTERN *AND* EASTERN SAMOA!"
   "SWEET LAMPREY OF SANTA FE!"
   "SWEET ROBOT SWAN OF BOTSWANA!"
   "SWEET EGO OF MONTEGO!"
   "SWEET CANDELABRA OF LA HABRA, LaBARBARA!"
   "SWEET TORNADOES OF BARBADOS!"
   ])

; %1$s is nick, %2$s is question, %3$s is answer
(def WIN-RESPONSES
  ["%1$s answered the question `%2$s` correctly with a response like `%3$s`"
   "%1$s got it by answering something like `%3$s` for the question `%2$s`"
   "%1$s got it by giving an answer close enough to `%3$s`!"
   "%1$s won this round with an answer close enough to `%3$s`!"
   "%1$s's answer was close enough to `%3$s`!"
   ])

; maybe also some fake "sponsored by" messages?
; brought to you by the AT Trivia Management Office

(comment
  (deref the-state)
  (reset! the-state {})
  (-> @the-state (get-in ["irc-freenode.net" "#silly-bot-test"]))
  (is-answer? (-> @the-state (get-in ["irc.freenode.net" "#silly-bot-test" :question])) "bitsbot: arclite shock cannon")
  (fetch :trivia)
  (use 'clojure.pprint)
  (pprint (frequencies (map :winner (fetch :trivia :where {:channel "#bitswebteam"}))))
  (->> (fetch :trivia :where {:channel "#bitswebteam"})
      (map :winner)
      (map (fn [nick] (or nick "<NO WINNER>")))
      (frequencies)
      (sort-by second)
      (reverse)
      (map (partial apply format "%s: %d"))
      (string/join ", "))

  )

; why the crap is this not in clojure.core?
(defn dissoc-in
  "Dissociates an entry from a nested associative structure where ks is a
  sequence of keys and returns a new nested structure."
  [m [k & ks]]
  (if ks
    (assoc m k (dissoc-in (get m k) ks))
    (dissoc m k)))

(defn parse-questions [buf]
  (if (re-matches #".*\*\*.*" buf)
    (for [line (string/split buf #"\*\*")]
      (let [[prompt answers num0 num1 num2 num3 category] (string/split line #"\|")]
        {:prompt prompt
         :answers (string/split answers #"\/")
         :category category
         ; what are these other numbers?  difficulty rating?  and what else?
         :mysteries [num0 num1 num2 num3]
         }))
    []))

(defn fetch-questions!
  ([] (fetch-questions! DEFAULT-CATEGORIES))
  ([cats] (->
            (str TRIVIA-URL (java.net.URLEncoder/encode cats))
            client/get
            :body
            parse-questions)))

(defn is-answer? [ques line]
  ; TODO: check that they are addressing the bot
  ; TODO: https://en.wikipedia.org/wiki/Approximate_string_matching
  ; maybe SOUNDEX or Levenshtein?
  (let [regexes (for [ans (:answers ques)]
                  (re-pattern (str "(?i).*" ans ".*")))]
    (some #(re-matches % line) regexes)))

(defn game-record
  "create a game record to store the current game results in the DB"
  [{:keys [channel nick message] :as com-m} ques won?]
  {:question ques
   :when     (now)
   :server   (get-server com-m)
   :channel  channel
   :winner   (if won? nick nil)
   })

(defn save-game-result! [com-m ques won?]
  (insert! :trivia (game-record com-m ques won?)))

(defn question-announcement [ques]
  (str "[" (:category ques) "] " (:prompt ques)))

(defn win-announcement [ques nick message]
  ; lame to just assume first answer...
  (str (rand-nth EXCLAMATIONS)
       " "
       (format (rand-nth WIN-RESPONSES)
               nick
               (:prompt ques)
               (first (:answers ques)))))

(comment
  (win-announcement {:prompt "what's a thing?" :answers ["a thing", "a different thing"]}
                    "joebob"
                    nil
                    )
  )
(defn failure-announcement [{:keys [prompt answers]}]
  (str "Nobody answered the question `" prompt "`. "
       (if (< 1 (count answers))
         (str "Acceptable answers: " (string/join " / " answers))
         (str "The answer was `" (first answers) "'."))))


; if we are asking trivia, we'll have keys for the current server/channel in our state atom
(def the-state (atom {}))

(defn game-running?
  "examine a com-m structure and decide if it originates
  from a channel with a running trivia game."
  [{:keys [channel] :as com-m}]
  (get-in @the-state [(get-server com-m) channel]))

(defn with-game
  "Run function `f` against the relevant game state, if there is any.
  The game state will be exclusively locked while `f` runs.
  Replace the game record with the result of `f`.
  If `f` returns nil, any associated future object will be canceled,
  and the relevant game state will be deleted."
  [{:keys [channel] :as com-m} f]
  (let [server (get-server com-m)]
    (swap! the-state
           (fn [st]
             (if-let [game (get-in st [server channel])]
               (if-let [result (f game)]
                 (assoc-in st [server channel] result)
                 (do ; if the game has a future thread, cancel it
                   (when (:future game)
                     (println "canceling future for game" game)
                     (future-cancel (:future game)))
                   (dissoc-in st [server channel])))
               st)))))

(defn start-game! [{:keys [channel args] :as com-m}]
  (if-let [questions (not-empty (fetch-questions! (string/join " " args)))]
    (let [ques      (rand-nth questions)]
      (swap! the-state
             assoc-in [(get-server com-m) channel]
             {:question ques
              :ask-time (now)
              })
      (let [fut (future
                  (dotimes [n PROMPTS]
                    (with-game com-m
                      (fn [game]
                        (send-message com-m (question-announcement ques))
                        (Thread/sleep PROMPT-INTERVAL)
                        game)))
                  (with-game com-m
                    (fn [game]
                      (send-message com-m (failure-announcement ques))
                      (save-game-result! com-m (:question game) false)
                      nil ; terminate the game
                      )))]
        (with-game com-m (fn [game] (assoc game :future fut)))))
    (send-message com-m "Sadly, snapnjacks does not recognize that category.")
    ))

(defn update-game! [{:keys [channel nick message] :as com-m}]
  (with-game com-m
    (fn [game]
      (println "got a game:" game)
      (let [ques (:question game)]
        (if (is-answer? ques message)
          (do (println nick "won the game with answer:" message)
              (send-message com-m (win-announcement ques nick message))
              (save-game-result! com-m ques true)
              nil) ; terminate game
          game ; leave game unchanged
          )))))

(defplugin
  (:hook :on-message update-game!)
  (:cmd
    "Start a trivia game. Optionally provide part of a category name from http://snapnjacks.com/qstats.php. See also @trivia-scores"
    #{"trivia"}
    (fn [{:keys [nick] :as com-m}]
      (showing-exceptions com-m
        (cond
          (game-running? com-m)
          (send-message com-m
                        (prefix nick "How are you unable to see that I'm already running a trivia game in here?"))
          :else
          (do
            (send-message com-m
                          (str nick " has started a round of trivia!  To answer, simply speak into the channel."))
            (start-game! com-m))))))
  (:cmd
    "A super lazy piece of programming that craps out some trivia scores"
    #{"trivia-scores" "triviascores" "trivia-stats" "triviastats"}
    (fn [{:keys [nick channel] :as com-m}]
      (showing-exceptions com-m
                          (send-message com-m (str "Trivia scores: "
                                                   (->> (fetch :trivia :where {:channel channel})
                                                        (map :winner)
                                                        (map (fn [nick] (or nick "<NO WINNER>")))
                                                        (frequencies)
                                                        (sort-by second)
                                                        (reverse)
                                                        (map (partial apply format "%s: %d"))
                                                        (string/join ", ")))))))

  ; TODO: skip question, cancel trivia
  ; add question (to local store)
  ; admin-only game-really-won-by <nick>
  ; DONE: forbid new @trivia if one is already going
  ; TODO: set a cap on the number of concurrent trivia threads
  ; TODO: compare candidate answers for levenshtein distance, tolerate small differences
  ; TODO: disconfirm wrong answers?  maybe not if we don't require addressing the bot
  ; DONE: do not require the bot to be addressed
  )

