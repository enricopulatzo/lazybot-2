(ns lazybot.plugins.uptime
  (:use [lazybot registry])
  (:use [clojure.java.shell :only [sh]])
  (:require [clj-time.core :as t])
  (:require [clojure.string :as string]))

(defn prefix-nick 
  [nick & stuff]
  (apply str nick ": " stuff))

(comment
  (use 'lazybot.core)
  (-> lazybot.core/bots
      deref
      vals
      first
      :bot
      deref
      :start-time
      )
  )

(defn bot-uptime-interval [com-m]
  (-> com-m
      :bot
      deref
      :start-time
      (t/interval (t/now))))

; been reading a lot of vernor vinge lately
(defn qeng-ho-time-units 
  [secs]
  (let [[exponent units] (condp < secs
                           1e6                      [6 "Msec"]
                           1e3                      [3 "ksec"]
                           Double/NEGATIVE_INFINITY [0 "seconds"]
                           )]
    [(/ secs (Math/pow 10 exponent)) units])
  )

(defn bot-uptime [com-m]
  (let [interval  (bot-uptime-interval com-m)
        secs      (t/in-secs interval)
        [t units] (qeng-ho-time-units secs)
        ]
    (str t " " units)))

; (bot-uptime com-m)

; this parsing is pretty ad-hoc and brittle
(defn show-uptime []
  (let [[days mins] (-> (sh "uptime")
                      :out
                      (string/split #",")
                      (->> (take 2))
                      ) 
        days       (-> days
                     (string/split #" ") 
                     (->> 
                       (drop 2)
                       (string/join " ")))
        mins      (string/trim mins) ]
    ; and i've been running since ... (extract :start-time from bot ref)
    (str "My host machine has been " days ", " mins "")))

; (show-uptime)

(defplugin
  (:cmd
    "see how long the bot's host machine has been up"
    #{"host-uptime"}
    (fn [{:keys [nick args] :as com-m}]
      (send-message com-m (prefix-nick nick (show-uptime)))))
  (:cmd
    "see how long the bot has been up"
    #{"uptime"}
    (fn [{:keys [nick args] :as com-m}]
      (send-message com-m (prefix-nick nick (str "I have been awake for " (bot-uptime com-m) ".")))))
  )

