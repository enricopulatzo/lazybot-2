(ns lazybot.plugins.anagram 
  (:use [lazybot registry]
        [lazybot.utilities :only [prefix]]
        [clojure.data.json :only [read-json]]) 
  (:require [clj-http.client :as client]
            [clojure.string :as str]
            )
  (:import org.apache.commons.lang.StringEscapeUtils)
  )

(def TARGET-URL "http://wordsmith.org/anagram/anagram.cgi?t=10&a=n&anagram=")

(defn strip-newlines [s]
  (clojure.string/replace s #"\n" ""))

 
 (defn scrape [input]
   (->>
     (ring.util.codec/url-encode input)
     (str TARGET-URL)
     (client/get)
     :body
     (strip-newlines)
     (re-matches #".*</b><br>(.*)") 
     (last)
     (#(str/split % #"<br>"))
     (take 10)
  )) 



(defn random-anagram-item
  "pull a random anagram from the results"
  [x] 
  (-> 
    (scrape x)
    rand-nth
    ))


(defplugin
  (:cmd
    "Anagrams, not Banagrams!"
    #{"anagram"}
    (fn [{:keys [nick args] :as  com-m}]
      (try
          (send-message com-m (prefix nick (random-anagram-item (str/join " " args))))
        (catch Exception e
          (send-message com-m (str "sorry, got an exception solving your anagram: " e)))))))


(comment
  
  (scrape "andy thompson")
  (random-anagram-item "andy thompson")

  )
