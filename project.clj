(defproject lazybot "0.7.0-alpha1"
  :description "FIXME: write"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [commons-lang/commons-lang "2.6"]
                 [commons-io/commons-io "1.4"]
                 [com.google.guava/guava "18.0"]
                 [backtype/clj-time "0.3.2"]
                 [clojail "1.0.6"]
                 ;; [clj-http "0.2.6"] ; very old
                 [clj-http "1.0.0"] 
                 [irclj "0.4.1"]
                 [congomongo "0.1.9"]
                 [clj-config "0.2.0"]
                 [compojure "0.6.5"]
                 [ring/ring-jetty-adapter "1.0.0-beta2"]
                 [log4j "1.2.17"]
                 [org.thnetos/cd-client "0.3.3"]
                 [org.jruby/jruby "1.6.5"]
                 [clj-wordnik "0.1.0-alpha1"]
                 [org.clojure/data.json "0.2.5"]
                 [org.clojure/tools.logging "0.2.3"]
                 [org.clojure/data.zip "0.1.1"]
                 [org.clojure/tools.cli "0.1.0"]
                 [useful "0.8.8"]
                 [hobbit "0.1.0-alpha1"]
                 [ororo "0.1.0"]
                 [socrates "0.0.1"]
                 [innuendo "0.1.7"]
                 [frinj "0.2.5"]
                 [tentacles "0.2.7"]
                 [findfn "0.1.3"]
                 [cheshire "4.0.2"]
                 ; adding these so i can play with them with ,() in the chan
                 [org.clojure/core.match "0.2.2"]
                 [org.clojure/core.logic "0.8.9"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [org.clojure/core.typed "0.2.72"]
                 [lonocloud/synthread "1.0.5"]
                 [frak "0.1.6"]  ; infers regexes from examples
                 [swiss-arrows "1.0.0"]
                 [clj-time "0.8.0"]
                 ]
  :uberjar-name "lazybot.jar"
  :main lazybot.run
  :copy-deps true
  :resources-path "resource"
  :jvm-opts ["-Djava.security.manager" 
             "-Djava.security.policy=lazybot.java.policy"
             ; trying this to save time
             "-noverify"
             ; got these from lazybot-opt script
             "-XX:ReservedCodeCacheSize=6m" 
             "-Xms10m" 
             "-Xmx120m"
             ])
