(ns lazybot.plugins.number-trivia
  (:require [clojure.string :as string])
  (:use [lazybot registry utilities])
  (:require [clj-http.client :as client]))


; http://numbersapi.com/
; from API docs:
;;; Just hit http://numbersapi.com/number/type to get a plain text response, where
;;;     type is one of trivia, math, date, or year. Defaults to trivia if omitted.
;;;     number is
;;;         - an integer, or
;;;         - the keyword random, for which we will try to return a random available fact, or
;;;         - a day of year in the form month/day  (eg. 2/29, 1/09, 04/1), if type is date
;;;         - ranges of numbers

(def BASE-URL "http://numbersapi.com/")

(defn api-request [& path-pieces]
  (-> (client/get (apply str BASE-URL path-pieces))
      :body))

(defn get-fact [num type]
  (println "got request for fact: " num type)
  (try
    (api-request num "/" type)
    (catch Throwable t
      (str "Dang, caught a Throwable! " t))))

(defplugin
  (:cmd
    "Show an arguably fun fact about a number.  Facthood is also arguable.  Defaults to a random number.  Unlock nerd mode with --nerd. `@number [--nerd] <num>`"
    #{"number" "number-trivia"}
    (fn [{:keys [nick args] :as com-m}]
      (if (= "--nerd" (first args))
        (send-message com-m (get-fact (or (first (rest args)) "random") "math"))
        (send-message com-m (get-fact (or (first args) "random") "trivia"))) ))
  (:cmd
    "Show a some kind of trivial 'fun fact' about a year.  If unspecified, I'll pick a random year.  @year <year>"
    #{"year" "year-trivia"}
    (fn [{:keys [nick args] :as com-m}]
      (send-message com-m (get-fact (or (first args) "random") "year"))))
  (:cmd
    "Show a somewhat fun fact about a date.  If unspecified, I'll pick a random date.  @date <month>/<day>"
    #{"date" "date-trivia"}
    (fn [{:keys [nick args] :as com-m}]
      (let [arg (or (first args) "random")]
        (cond
          (or (= arg "random") (re-matches #"\d{1,2}\/\d{1,2}" arg))
          (send-message com-m (get-fact arg "date"))
          ; TODO: map MM-DD -> MM/DD ?

          (re-matches #"(?i)your mom" arg)
          (send-message com-m (prefix nick "Har har har."))

          :else
          (send-message com-m (prefix nick "The date format has to be MM/DD, or leave it blank for a random date."))
          ))))
  )

(comment
  (re-matches #"foo" nil)
  (re-matches #"\d{1,2}\/\d{1,2}" "03/25")
  )

