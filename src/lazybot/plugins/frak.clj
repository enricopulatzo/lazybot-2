(ns lazybot.plugins.frak
  (:require [clojure.string :as string]
            [frak]
            )
  (:use [lazybot registry]
        [somnium.congomongo :only [fetch fetch-one insert! destroy! update!]]))

(defn prefix-nick
  [nick & stuff]
  (apply str nick ": " stuff))

(defplugin
  (:cmd
    "infer a regular expression from examples. syntax: @pattern example1 example2 example3..."
    #{"pattern"}
    (fn [{:keys [nick args] :as com-m}]
      (let [pat (frak/pattern args)]
        (send-message com-m (prefix-nick nick pat)))))
  )
