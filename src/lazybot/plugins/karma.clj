; Written by Michael D. Ivey <ivey@gweezlebur.com>
; Licensed under the EPL

(ns lazybot.plugins.karma
  (:use [lazybot registry info]
        [lazybot.plugins.login :only [when-privs]]
        [useful.map :only [keyed]]
        [somnium.congomongo :only [fetch fetch-one insert! update!]])
  (:require [clojure.string :as string])
  )

(defn- key-attrs [nick server channel]
  (let [nick (.toLowerCase nick)]
    (keyed [nick server channel])))

(defn- set-karma
  [nick server channel karma]
  (let [attrs (key-attrs nick server channel)]
    (update! :karma attrs (assoc attrs :karma karma))))

(comment
  (map :nick (fetch :karma))
  (set-karma "foo" "irc.freenode.net" "bitsbot" 123)
  (get-karma "foo" "irc.freenode.net" "bitsbot")
  (deref limit)
  )

(defn- get-karma
  [nick server channel]
  (let [user-map (fetch-one :karma
                            :where (key-attrs nick server channel))]
    (get user-map :karma 0)))

(defn- get-random-karma []
  (let [{:keys [nick karma]} (rand-nth (fetch :karma))]
    (str nick " has karma score " karma)))

(def limit (ref {}))

;; TODO: mongo has atomic inc/dec commands - we should use those
(defn- change-karma
  [snick new-karma {:keys [nick com bot channel] :as com-m}]
  (let [[msg apply]
        (dosync
         (let [current (get-in @limit [nick snick])]
           (cond
            (= nick snick) ["You can't adjust your own karma."]
            (= current 3) ["Do I smell abuse? Wait a while before modifying that person's karma again."]
            (= current new-karma) ["You want me to leave karma the same? Fine, I will."]
            :else [(str (get-in @bot [:config :prefix-arrow]) new-karma)
                   (alter limit update-in [nick snick] (fnil inc 0))])))]
    (when apply
      (set-karma snick (:server @com) channel new-karma)
      (future (Thread/sleep 300000)
              (println "karma limit decrementer future has sprung into action for" nick "and" snick "!")
              (dosync (alter limit update-in [nick snick] dec))))
    (send-message com-m msg)))

(defn karma-fn
  "Create a plugin command function that applies f to the karma of the user specified in args."
  [f]
  (fn [{:keys [com channel args] :as com-m}]
    (let [snick (first args)
          karma (get-karma snick (:server @com) channel)
          new-karma (f karma)]
      (change-karma snick new-karma com-m))))


(comment
  (def chan "#bitswebteam")
  (def limit 5)
  (def kind :best)
  (def kind :worst)
  (doseq [{:keys [nick karma]} (fetch :karma 
                                      :where {:channel chan}
                                      :sort {:karma (if (= kind :best) -1 1)}
                                      :limit limit
                                      )]
    (println (str nick " has karma " karma ".")))

  (defn dummy-send-message [_ s]
    (println s))

  (with-redefs [send-message dummy-send-message]
               (karma-report nil "#bitswebteam" 10 :best)
               )
  )

(defn karma-report [com-m chan limit kind]
  ; could also verify server is the same but m'eh
  (doseq [{:keys [nick karma]} (fetch :karma 
                                      :where {:channel chan}
                                      :sort {:karma (if (= kind :best) -1 1)}
                                      :limit limit) ]
    (send-message com-m (str nick " has karma " karma "."))))

(defplugin
  (:hook :on-message
         (fn [{:keys [message] :as com-m}]
           (let [[_ direction snick] (re-find #"^\((inc|dec) (.+)\)(\s*;.*)?$" message)]
             (when snick
               ((karma-fn (case direction
                                "inc" inc
                                "dec" dec))
                (merge com-m {:args [snick]}))))))
  (:cmd
    "Generates a simple report of the best karma scorers"
    #{"karmabest"}
    (fn [{:keys [channel] :as com-m}]
      (karma-report com-m channel 5 :best)))

  (:cmd
    "Generates a simple report of the worst karma scorers"
    #{"karmaworst"}
    (fn [{:keys [com channel args] :as com-m}]
      (karma-report com-m channel 5 :worst)))

  (:cmd
   "Checks the karma of the person you specify."
   #{"karma"}
   (fn [{:keys [com bot channel args] :as com-m}]
     (let [nick (string/join " " args)] ; [nick (first args)]
       (send-message com-m
                     (if-let [karma (get-karma nick (:server @com) channel)]
                       (str nick " has karma " karma ".")
                       (str "I have no record for " nick "."))))))
  (:cmd
   "Increases the karma of the person you specify."
   #{"inc"} (karma-fn inc))
  (:cmd
   "Decreases the karma of the person you specify."
   #{"dec"} (karma-fn dec))
  (:indexes [[:server :channel :nick]])
  
  (:cmd
    "Force the karma of someone to a specific value. Admins only. force-karma <score> <thing>"
    #{"force-karma"}
    (fn [{:keys [nick channel args com] :as com-m}]
      (when-privs com-m :admin
        (let [server               (:server @com)
              [score & name-parts] args
              score                (Integer. score)
              target               (string/join " " name-parts) ] 
          (set-karma target server channel score)
          (send-message com-m 
                        (str nick ": " target " now has karma " 
                             (get-karma target server channel)))))))
  (:cmd
    "Display a random karma score, in a futile attempt to assuage your boredom"
    #{"random-karma" "randomkarma"}
    (fn [com-m]
      (send-message com-m (get-random-karma))))
  
  )

